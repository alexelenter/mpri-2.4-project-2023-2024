(* A type of explicitly-typed terms. *)

module TyVar = Structure.TyVar

type 'v ty_ =
  | Constr of ('v, 'v ty_) Structure.t_

(* type 'v ty =
  | Var of 'v
  | Arrow of 'v ty * 'v ty
  | Prod of 'v ty list *)

type raw_ty = string ty_
type ty = TyVar.t ty_

(* type ty =
  | Var of TyVar.t
  | Arrow of TyVar.t ty * TyVar.t ty
  | Prod of TyVar.t ty list *)

let rec freshen_ty (Constr s) =
  Constr (Structure.freshen freshen_ty s)

module TeVar = Utils.Variables ()

type value = 
| Int of int
| Bool of bool

type term =
  | Val of value
  | Var of TeVar.t
  | App of term * term
  | Abs of TeVar.t * ty * term
  | Let of TeVar.t * ty * term * term
  | Annot of term * ty
  | Tuple of term list
  | LetTuple of (TeVar.t * ty) list * term * term
