(*
   As explained in the README.md ("Abstracting over an effect"),
   this module as well as other modules is parametrized over
   an arbitrary effect [T : Functor].
*)

module Make (T : Utils.Functor) = struct
  module Constraint = Constraint.Make(T)
  module SatConstraint = SatConstraint.Make(T)
  module ConstraintSimplifier = ConstraintSimplifier.Make(T)
  module ConstraintPrinter = ConstraintPrinter.Make(T)

  type env = Unif.Env.t
  type log = PPrint.document list

  let make_logger c0 =
    let logs = Queue.create () in
    let c0_erased = SatConstraint.erase c0 in
    let add_to_log env =
      let doc =
        c0_erased
        |> ConstraintSimplifier.simplify env
        |> ConstraintPrinter.print_sat_constraint
      in
      Queue.add doc logs
    in
    let get_log () =
      logs |> Queue.to_seq |> List.of_seq
    in
    add_to_log, get_log

  (** See [../README.md] ("High-level description") or [Solver.mli]
      for a description of normal constraints and
      our expectations regarding the [eval] function. *)
  type ('a, 'e) normal_constraint =
    | NRet of 'a Constraint.on_sol
    | NErr of 'e
    | NDo of ('a, 'e) Constraint.t T.t

  let rec myeval : type a e. env -> (a, e) Constraint.t -> _ -> _ -> (_ * (a, e) normal_constraint)
  = fun env c0 add_to_log get_log ->
    match c0 with
    | Ret on_sol  -> (env, NRet on_sol)
    | Err e       -> (env, NErr e)
    | Map (c1, f) -> 
      let (env, c) = myeval env c1 add_to_log get_log in
      (match c with
      | NRet c  -> (env, NRet (fun env -> f (c env)))
      | NErr e  -> (env, NErr e)
      | NDo cs  -> (env, NDo (T.map (fun c -> Constraint.Map(c, f)) cs)))
    | MapErr (c1, f) -> 
      let (env, c) = myeval env c1 add_to_log get_log in
      (match c with
      | NRet c  -> (env, NRet c)
      | NErr e  -> (env, NErr (f e))
      | NDo cs  -> (env, NDo (T.map (fun c -> Constraint.MapErr(c, f)) cs)))
    | Conj (c1, c2) ->
      let (env', cres1) = myeval env c1 add_to_log get_log in
      let (env, cres2) = myeval env' c2 add_to_log get_log in
      (match (cres1, cres2) with
      | (NRet cres1, NRet cres2)   -> (env, NRet (fun env -> (cres1 env, cres2 env)))
      | (NErr e, _) | (_, NErr e)  -> (env, NErr e)
      | (NDo cs, NRet _)           -> 
        (env, NDo (T.map (fun c -> Constraint.Conj(c, c2)) cs))
      | (NRet _, NDo cs)           -> 
        (env, NDo (T.map (fun c -> Constraint.Conj(c1, c)) cs))
      | (NDo cs1, NDo cs2)         -> 
        (env, NDo (T.map (fun c -> Constraint.Conj(c, Do cs2)) cs1)))
    | Eq (v1, v2) ->
      let res = Unif.unify env v1 v2 in
      (match res with
      | Ok env               -> add_to_log env;(env, NRet (fun _ -> ()))
      | Error Clash (v1, v2) -> (env, NErr (Clash (Decode.decode env v1, Decode.decode env v2)))
      | Error Cycle v        -> (env, NErr (Cycle v)))
    | Exist (w, structure, c1) -> 
      let env = Unif.Env.add w structure env in
      add_to_log env;
      let (env, c) = myeval env c1 add_to_log get_log in
      (match c with
      | NRet on_sol -> (env, NRet on_sol)
      | NErr error  -> (env, NErr error)
      | NDo cs      -> (env, NDo (T.map (fun c -> Constraint.Exist(w, structure, c)) cs)))
    | Decode v -> (env, NRet (fun _ -> Decode.decode env v))
    | Do constraints -> (env, NDo constraints)

  let eval (type a e) ~log (env : env) (c0 : (a, e) Constraint.t)
    : log * env * (a, e) normal_constraint
  =
    let add_to_log, get_log =
      if log then make_logger c0
      else ignore, (fun _ -> [])
    in
    (* We recommend calling the function [add_to_log] above
       whenever you get an updated environment. Then call
       [get_log] at the end to get a list of log message.

       $ dune exec -- minihell --log-solver foo.test

       will show a log that will let you see the evolution
       of your input constraint (after simplification) as
       the solver progresses, which is useful for debugging.

       (You can also tweak this code temporarily to print stuff on
       stderr right away if you need dirtier ways to debug.)
    *)
    let (env, res) = myeval env c0 add_to_log get_log in 
    (get_log (), env, res)
end
