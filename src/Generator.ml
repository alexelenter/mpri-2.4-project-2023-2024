module Make(M : Utils.MonadPlus) = struct
  module Untyped = Untyped.Make(M)
  module Constraint = Constraint.Make(M)
  module Infer = Infer.Make(M)
  module Solver = Solver.Make(M)
  module ConstraintPrinter = ConstraintPrinter.Make(M)


  (* just in case... *)
  module TeVarSet = Untyped.Var.Set
  module TyVarSet = STLC.TyVar.Set


let untyped : Untyped.term =
  (* This definition is *not* a good solution,
     but it could give you a flavor of possible definitions. *)
  let rec gen vars : Untyped.term =
    let open Untyped in
    Do (M.delay @@ fun () ->
      let x = Untyped.Var.fresh "x" in
      let x2 = Untyped.Var.fresh "x" in
      M.sum [
        M.sum (List.map (fun var -> M.return (Var var)) (TeVarSet.elements vars));
        M.delay @@ (fun () -> 
          let next = gen vars in
          let next1 = gen (TeVarSet.add x vars) in
          let next2 = gen (TeVarSet.add x2 (TeVarSet.add x vars)) in
          M.sum [
            M.return (LetTuple ([x;x2], next, next2));
            M.return (App(next, next)); 
            M.return (Abs (x, next1));
            M.return (Tuple [next; next]);
            M.return (Let (x, next, next1));
          ])
      ]
    )  
  in gen Untyped.Var.Set.empty

let constraint_ : (STLC.term, Infer.err) Constraint.t =
  let w = Constraint.Var.fresh "final_type" in
    Constraint.(Exist (w, None,
    Infer.has_type
      Untyped.Var.Map.empty
      untyped
      w))

let extract_terms seq env =
  let extract_term c = 
    match Solver.eval ~log:false env c with
    | (_, _, NRet on_sol) -> M.return (on_sol (Decode.decode env))
    | (_, _, _)           -> M.fail
  in
  M.bind seq extract_term 

let typed ~depth =
  (* This definition uses [constraint_] to generate well-typed terms.
     An informal description of a possible way to do this is described
     in the README, Section "Two or three effect instances", where
     the function is valled [gen]:

     > it is possible to define a function
     >
     >     val gen : depth:int -> ('a, 'e) constraint -> ('a, 'e) result M.t
     >
     > on top of `eval`, that returns all the results that can be reached by
     > expanding `Do` nodes using `M.bind`, recursively, exactly `depth`
     > times. (Another natural choice would be to generate all the terms that
     > can be reached by expanding `Do` nodes *at most* `depth` times, but
     > this typically gives a worse generator.)
  *)
  let rec typed_rec depth (c : (STLC.term, Infer.err) Constraint.t) =
    let (_, env, normal_constraint) = Solver.eval ~log:false Unif.Env.empty c in
      match normal_constraint with
      | NRet on_sol -> 
        if depth = 0 then M.return (on_sol (Decode.decode env)) else M.fail
      | NErr _      -> M.fail
      | NDo cs      -> 
        if depth = 2 
        then 
          extract_terms cs env
        else 
          let a = M.bind cs (fun c -> typed_rec (pred depth) c) in a
  in typed_rec depth constraint_  
end
