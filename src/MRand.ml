type 'a t = 'a Seq.t

let map (f : 'a -> 'b) (s : 'a t) : 'b t =
  Seq.map f s

let return (x : 'a) : 'a t =
  Seq.return x

let bind (sa : 'a t) (f : 'a -> 'b t) : 'b t =
  Seq.map f sa |> Seq.concat

let delay (f : unit -> 'a t) : 'a t =
  (fun () -> (f ()) ())

let knuth_shuffle a =
  let n = Array.length a in
  let a = Array.copy a in
  for i = n - 1 downto 1 do
    let k = Random.int (i+1) in
    let x = a.(k) in
    a.(k) <- a.(i);
    a.(i) <- x
  done;
  a

let sum (li : 'a t list) : 'a t =
  let a = Array.of_list li in
  let a = knuth_shuffle a in
  Array.fold_left (fun acc a -> Seq.append acc a) Seq.empty a

let fail : 'a t = 
  Seq.empty

let one_of (vs : 'a array) : 'a t =
  if Array.length vs = 0 then fail else Random.int (Array.length vs) |> Array.get vs |> return

let run (s : 'a t) : 'a Seq.t =
  let array = Array.of_seq s in
  let choose = (fun a -> Random.int (Array.length a) |> Array.get a) in
  let rec res = fun () -> Seq.Cons (choose array, res) in
  res
