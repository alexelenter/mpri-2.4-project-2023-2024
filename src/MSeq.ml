type 'a t = 'a Seq.t

let map (f : 'a -> 'b) (s : 'a t) : 'b t =
  Seq.map f s

let return (x : 'a) : 'a t =
  Seq.return x

let bind (sa : 'a t) (f : 'a -> 'b t) : 'b t =
  Seq.map f sa |> Seq.concat

let delay (f : unit -> 'a t) : 'a t =
  (fun () -> (f ()) ())
  
let sum (li : 'a t list) : 'a t =
  List.fold_left (fun acc a -> Seq.append acc a) Seq.empty li 

let fail : 'a t =
  Seq.empty

let one_of (vs : 'a array) : 'a t =
  if Array.length vs = 0 then fail else Array.get vs 0 |> return

let run (s : 'a t) : 'a Seq.t =
  s
